# Pull official base image
FROM node:12.16.2 as react-build

# Set working directory 
WORKDIR /app

# Install app dependencies
COPY . ./
RUN yarn
RUN yarn build

# stage: 2 — the production environment
FROM nginx:alpine
COPY — from=react-build /app/build /usr/share/nginx/html
EXPOSE 80
CMD [“nginx”, “-g”, “daemon off;”]

