import React, { Component } from 'react';

import TodoItem from './components/TodoItem';
import './styles/style.css';
import todosData from './todosData';

class App extends Component {
  constructor() {
    super();
    this.state = {
      todos: todosData,
    }
    this.handleChange = this.handleChange.bind(this);
  }

  // componentDidMount() {

  // }

  // shouldComponentUpdate(nextProps, nextState) {
    // return true if want it to update
    // return false if not
  // }

  // componentWillUnmount() {

  // }


  handleChange(id) {
    this.setState((prevState) => {
      let updatedTodos = prevState.todos.map(todo => {
        if (todo.id === id) {
          return {
            ...todo,
            completed: !todo.completed
          }
        }
        return todo;
      })
      return { todos: updatedTodos }
    })
  }

  render() {
    let data = this.state.todos.map(item => <TodoItem key={item.id} content={item} handleChange={this.handleChange} />)

    return (
      <div className='todo-list'>
        {data}
      </div>
    )
  }
}

export default App;
