import React from 'react';
import '../styles/style.css'

function TodoItem(props) {
  const completedStyle = {
    fontStyle: "italic",
    color: "#cdcdcd",
    textDecoration: "line-through"
  }

  return (
    <div className="todo-item">
      <input
        type="checkbox"
        checked={props.content.completed}
        onChange={() => {
            props.handleChange(props.content.id)
          }
        }
      />
      <p style={props.content.completed ? completedStyle : null}>{props.content.text}</p>
    </div>
  )
}

export default TodoItem;
